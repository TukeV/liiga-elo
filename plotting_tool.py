from matplotlib import pyplot as plt
from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from liiga_elo import STARTING_POINTS as starting_points
import ftfy
import numpy as np
import os

"""
First there are colors what plotter will use for each team. These are handpicked values, 
so if you want to tailor the graph for your own tastes, then you be prepared to so some manual labor.
"""

"""Colors for lines used on the graph."""
LINE_COLORS = {
    ftfy.fix_encoding("Kärpät"): "black",
    ftfy.fix_encoding("HIFK"): "red",
    ftfy.fix_encoding("Tappara"): "blue",
    ftfy.fix_encoding("Pelicans"): "cyan",
    ftfy.fix_encoding("TPS"): "black",
    ftfy.fix_encoding("Lukko"): "gold",
    ftfy.fix_encoding("HPK"): "darkorange",
    ftfy.fix_encoding("Ilves"): "green",
    ftfy.fix_encoding("JYP"): "black",
    ftfy.fix_encoding("SaiPa"): "yellow",
    ftfy.fix_encoding("KooKoo"): "black",
    ftfy.fix_encoding("Sport"): "red",
    ftfy.fix_encoding("KalPa"): "black",
    ftfy.fix_encoding("Jukurit"): "blue",
    ftfy.fix_encoding("Ässät"): "red"
}

"""Colors used for each marker."""
FACE_COLORS = {
    ftfy.fix_encoding("Kärpät"): "gold",
    ftfy.fix_encoding("HIFK"): "blue",
    ftfy.fix_encoding("Tappara"): "orange",
    ftfy.fix_encoding("Pelicans"): "black",
    ftfy.fix_encoding("TPS"): "white",
    ftfy.fix_encoding("Lukko"): "blue",
    ftfy.fix_encoding("HPK"): "black",
    ftfy.fix_encoding("Ilves"): "yellow",
    ftfy.fix_encoding("JYP"): "red",
    ftfy.fix_encoding("SaiPa"): "black",
    ftfy.fix_encoding("KooKoo"): "orange",
    ftfy.fix_encoding("Sport"): "white",
    ftfy.fix_encoding("KalPa"): "gold",
    ftfy.fix_encoding("Jukurit"): "yellow",
    ftfy.fix_encoding("Ässät"): "black"
}

""" My version also draws emblems of the each team. However, since these logos are copyrighted material,
 I will not include them in my repository. You can download them from...
 * liiga website (www.liiga.fi)
 * Official websites of each team 
 * Wikipedia
"""
LOGOS = {
    ftfy.fix_encoding("Kärpät"): "./logos/karpat.png",
    ftfy.fix_encoding("HIFK"): "./logos/hifk.png",
    ftfy.fix_encoding("Tappara"): "./logos/tappara.png",
    ftfy.fix_encoding("Pelicans"): "./logos/pelicans.png",
    ftfy.fix_encoding("TPS"): "./logos/tps.png",
    ftfy.fix_encoding("Lukko"): "./logos/lukko.png",
    ftfy.fix_encoding("HPK"): "./logos/hpk.png",
    ftfy.fix_encoding("Ilves"): "./logos/ilves.png",
    ftfy.fix_encoding("JYP"): "./logos/jyp.png",
    ftfy.fix_encoding("SaiPa"): "./logos/saipa.png",
    ftfy.fix_encoding("KooKoo"): "./logos/kookoo.png",
    ftfy.fix_encoding("Sport"): "./logos/sport.png",
    ftfy.fix_encoding("KalPa"): "./logos/kalpa.png",
    ftfy.fix_encoding("Jukurit"): "./logos/jukurit.png",
    ftfy.fix_encoding("Ässät"): "./logos/assat.png"
}


def verify_logos():
    """
    Verifies that each logo asset actually exists. Returns true if they do and False if even one is missing.
    The function also prints which logs are missing.
    """
    all_exist = True
    for team, path in LOGOS.items():
        if not os.path.exists(path):
            print("Logo for {} cannot be found from {}".format(team, path))
            all_exist = False
    if not all_exist:
        print("Some logos do not exist. Logos will not be drawn.")
    return all_exist


class Plotter:
    """A helper class which plots the graph with matplotlib library."""
    def __init__(self, season_length):
        plt.ion()
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, facecolor="dimgray")
        self.team_data = {}
        self.ax.autoscale(True, 'y')
        # The figure is initialized so that the X axis is already big enough to fit all the games.
        self.ax.set_xlim(0, season_length + 10)
        self.logos_exist = verify_logos()

    def add_team(self, name):
        """Creates the line object for the team. Uses the color settings from LINE_COLORS and FACE_COLORS."""
        name = ftfy.fix_encoding(name)
        if name not in self.team_data:
            line, = self.ax.plot([0], [starting_points], 's-', label=name, color=LINE_COLORS[name], markerfacecolor=FACE_COLORS[name])
            self.team_data[name] = ([starting_points], line, None)

    def update_elo(self, team):
        """
        Updates the elo graph with the latest value.
        This will also draw the logo to the graph if all the image assets are present.
        """
        name = ftfy.fix_encoding(team.name)
        if team.games != len(self.team_data[name]):
            y_data, line, art = self.team_data[name]
            y_data.append(team.elo)
            line.set_ydata(y_data)
            tmp = np.arange(len(y_data))
            line.set_xdata(tmp)
            self.team_data[name] = (y_data, line, art)
            if self.logos_exist:
                self.update_logo(team.name, .2)

    def update_logo(self, team_name, zoom):
        """
        Removes the old logo picture and redraws it next to the latest marker.
        :param team_name: Name of the team, which we are updating.
        :param zoom: How big the zoom should be for the picture.
        """
        y_data, line, art = self.team_data[team_name]
        if art is not None:
            art.remove()
        image = OffsetImage(plt.imread(LOGOS[ftfy.fix_encoding(team_name)]), zoom=zoom)
        img = AnnotationBbox(image, (len(y_data)-1, y_data[-1]), frameon=False)
        artist = self.ax.add_artist(img)
        self.team_data[team_name] = y_data, line, artist

    def update(self):
        """Re-draws the graph and updates the Y-axis. This is only used if user wants to animate the graph."""
        self.ax.relim()
        self.ax.autoscale_view(True, True, True)
        plt.draw()
        plt.pause(.001)

    def stop(self):
        """This is the final draw function for the plot and will block until the graph window has been closed."""
        handles, labels = self.ax.get_legend_handles_labels()
        labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: -self.team_data[t[0]][0][-1]))
        labels = list(labels)
        for i in range(len(labels)):
            label = labels[i]
            elo_score = self.team_data[label][0][-1]
            labels[i] = "{0} - {1}".format(label, round(elo_score))
        self.ax.legend(handles, labels)
        if self.logos_exist:
            for team_name in self.team_data:
                self.update_logo(team_name, .2)
        plt.ioff()
        plt.show()

