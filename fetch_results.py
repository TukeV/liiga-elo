import requests
import ftfy
from bs4 import BeautifulSoup as BSoup


def get_season_data(url, output_file):
    """
    Scrapes the game results from the liiga website and saves them to the format used by liiga-elo.py.
    Please look into liiga.fi end-user terms before using. Also, there are no grantees if this will work if liiga
    decides to update their website.
    :param url: url where the html file can be found.
    :param output_file: File where the parsed result will be saved. The output format is csv, so you might want to make
    sure the output is a csv file as well.
    :return:
    """
    game_html = requests.get(url).text
    soup = BSoup(game_html, 'html.parser')
    all_games = soup.find('table', attrs={"id": "games"}).find('tbody').find_all('tr')
    with open(output_file, 'w') as fout:
        for game in all_games:
            try:
                cells = game.find_all("td")
                home, visitor = "".join(cells[3].find("a").text.split()).split('-')
                home_score, visitor_score = [x.strip() for x in cells[5].text.split('—')]
                overtime = cells[6].text.strip()
                fout.write(ftfy.fix_encoding("{}, {}, {}, {}, {}\n".format(home,visitor, home_score, visitor_score, overtime)))
            except ValueError:
                break


if __name__ == '__main__':
    # Fill these two parameters by yourself.
    get_season_data("url to games page", "path to the output file")
