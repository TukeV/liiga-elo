#!/usr/bin/env python
import operator
import ftfy
import plotting_tool

# Amount of points each team will start with.
STARTING_POINTS = 1500

# Value how much each game is worth in ELO score. In wikipedia examples this is 400
N_VALUE = 400

# The K multiplier during the season. This determines how aggressively algorithm will reward/punish for
# winning or losing against player with much different ELO score.
K_SEASON = 16

# K multiplier during the playoffs. At least in football the regular season games and tournament games (such as
# Champions league games) are evaluated with different score. After all, playoff games are much more serious games
# than regular season games.
K_PLAYOFF = 32

# Season length. After team has played this much games, the function will switch the K_SEASON to K_PLAYOFF
SEASON_LENGTH = 60

# Dictionary where games are stored.
TEAMS = {}


class Team:
    """This class keeps track on each team's elo score and number of games they have played."""
    def __init__(self, name):
        self.name = name
        self.elo = STARTING_POINTS
        self.games = 0

    def update_elo(self, opponent_rating, result):
        """
        Updates Elo score of the team and increases the number of games the team has played.
        More information about the algorithm can read for instance from wikipedia:
        https://en.wikipedia.org/wiki/Elo_rating_system

        :param opponent_rating: Elo rating of the opponent
        :param result: The result of the game as a decimal number between 0 and 1. The winning team gets 1 point,
        losing team gets 0 points and draw woould award both teams with .5 points.
        See score2points for more information.
        :return:
        """
        self.games += 1
        elo_difference = (opponent_rating - self.elo)
        tmp = elo_difference / N_VALUE
        estimate = 1/(1+pow(10,tmp))
        multiplier = K_SEASON if self.games <= SEASON_LENGTH else K_PLAYOFF
        # multiplier =  (16/900) * self.games * self.games - (16/15) * self.games + 32
        self.elo += multiplier * (result - estimate)


def score2points(home_score, visitor_score, overtime=False, playoff=False):
    """In liiga game points are awarded as following:
        * If the game ends in regular time, the winner gets 3 points and losing team gets nothing.
        * If the game ends in overtime, the winner gets 2 points and loser gets one point.
        * There are no draws.
        * In playoffs, only wins count. No scores are awarded.
    Therefore winning the game in regular time awards 100% of the points, ie the result for ELO algorithm is 1.0.
    Losing a game on regular time means that the result is 0.
    Winning the game on overtime yields 66% and losing yields 33% of the points. So the scores are 0.666 and 0.333
    """
    if home_score > visitor_score:
        return (.666, .333) if (overtime and not playoff) else (1.0, 0)
    else:
        return (.333, .666) if (overtime and not playoff) else (0, 1.0)


def update_elo(home_team, visitor_team, home_score, visitor_score, overtime=False):
    """Updates the Elo score for both teams. First we calculate the game result as in which game did win or lose and did
    the game end before the overtime. Then we update the elo score of the both teams.
    """
    home_points, visitor_points = score2points(home_score, visitor_score, overtime, home_team.games >= SEASON_LENGTH)
    home_rating = home_team.elo
    visitor_rating = visitor_team.elo
    home_team.update_elo(visitor_rating, home_points)
    visitor_team.update_elo(home_rating, visitor_points)


def parse_line(line, plotter=None):
    """Parses the lines. The input should have the following structure:
    {Name of the home team}, {name of the visitor}, {home team score}, {visitor score}, {string to indicate}
    This would have been easier and more efficient do with pandas or regular expression , but what can you do
    ¯\\_(ツ)_/¯ """
    if line[0] not in TEAMS:
        TEAMS[line[0]] = Team(line[0])
        plotter.add_team(line[0])
    if line[1] not in TEAMS:
        TEAMS[line[1]] = Team(line[1])
        plotter.add_team(line[1])
    line[0] = TEAMS[line[0]]
    line[1] = TEAMS[line[1]]
    line[2] = float(line[2])
    line[3] = float(line[3])
    line[4] = line[4].strip() != ''
    return line


def print_ratings():
    """Prints the team names and their elo score. The lines printed after 6th and 10th team mean the playoff lines
    in the liiga. In liiga, first 6 teams qualify immediately to the second round of the playoffs, while first round
    is played between teams in 7th, 8th, 9th and 10th."""
    counter = 0
    for team in (sorted(TEAMS.values(), key=operator.attrgetter('elo'), reverse=True)):
        print(ftfy.fix_encoding(team.name), int(team.elo))
        if counter == 5 or counter == 9:
            print('-----------------')
        counter += 1
    print('-----------------')


def run_file(filename, animate=True):
    """
    Reads the file, calculates the elo score and draws a graph describing how elo score of each team fluctuated during
    the season. See plotting_tool.py for more information about the plotting.
    :param filename: CSV file which contains the game info.
    :param animate: If True, the plotting tool will animate the graph. Otherwise only the final result is drawn.
    """
    my_plot = plotting_tool.Plotter(SEASON_LENGTH)
    with open(filename, 'r') as file:
        for line in file:
            game = line.split(',')
            game[0] = ftfy.fix_encoding(game[0].strip())
            game[1] = ftfy.fix_encoding(game[1].strip())
            game_result = parse_line(game, my_plot)
            update_elo(*game_result)
            my_plot.update_elo(game_result[0])
            my_plot.update_elo(game_result[1])
            if animate:
                my_plot.update()
    my_plot.update()
    print_ratings()
    my_plot.stop()


if __name__ == '__main__':
    import fetch_results
    fetch_results.get_season_data("https://liiga.fi/fi/ottelut/2019-2020/runkosarja/", "season1920.csv")
    run_file("season1920.csv", animate=True)
