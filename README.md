# Liiga Elo
## Exploring the [Elo rating system](https://en.wikipedia.org/wiki/Elo_rating_system) with [Liiga statistics](https://liiga.fi/en/) 

This is the repository for my hobby project, where I looked into inner workings of the Elo algorithm. In this project I used game scores from Finnish hockey league ["Liiga"](www.liiga.fi) to calculate and visualize Elo score for each team.

## Usage

This toy has not been intended for other than personal use, so you might have to do some manual code-stabbing to get this code working as intended.

### Downloading and converting season statistics

The game results can be found from Liiga website. If you enter the url of the season results and the resulting csv file path into the `fetch_results.py` script, it should automatically read and parse that page and generate required .csv file. The `example.csv` is an example of a correct csv file which has three made up games. 

### Running the script

The `liiga_elo.py` is the main code file, which contains the whole algorithm. It uses `Plotter` class from `plotting_tool.py` to visualize the results. By default, it reads and animates the games from `example.csv`, but since that file has only 3 games, the animations are rather short.

To change the file the script uses and if you want to use animations or not, you have to edit the `run_file` function call at the bottom of the script and replacing the `example.csv` with your own file. The boolean value for animation determines if the plot should be redrawn after every game.

### Adding logos

The `plotting_tool` can also draw the logos of each team on the graph, but because logos are immaterial property of their teams, I'm not going to add them to this repository. However, if you want to download them yourself, you can find them from wikipedia, liiga.fi or from each team's official website.
... Or you *could* make your own bootleg copies in paint if you want :)

You can find more instructions about the logos from the comments in `plotting_tool.py`.
 
## Licence 

[WTFPL](https://en.wikipedia.org/wiki/WTFPL), although it would be nice if you would give me shout out if you find this toy useful in anyway.
